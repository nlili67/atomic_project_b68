<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database;
use PDO;

class ProfilePicture extends Database
{
    public $id, $name, $profilePicture;


    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray["id"];

        if(array_key_exists("Name",$postArray))
            $this->name = $postArray["Name"];

        if(array_key_exists("ProfilePicture",$postArray))
            $this->profilePicture = $postArray["ProfilePicture"];


    }// end of setData Method


    public function store(){

        $sqlQuery = "INSERT INTO profile_picture ( name, profile_picture) VALUES ( ?, ?)";
        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->name, $this->profilePicture ];

        $status = $sth->execute($dataArray);

        if($status)

            Message::message("Success! Data has been inserted successfully<br>");
        else
            Message::message("Failed! Data has not been inserted<br>");

    }// end of store() Method

    public function index(){

        $sqlQuery = "SELECT * FROM profile_picture ORDER BY id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }// end of index() Method
    public function view(){

        $sqlQuery = "SELECT * FROM profile_picture WHERE id=".$this->id;

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData=  $sth->fetch();
        return $oneData;

    }// end of index() Method

}// end of ProfilePicture Class