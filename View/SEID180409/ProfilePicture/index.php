<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\ProfilePicture\ProfilePicture;


$obj = new ProfilePicture();

$allData  =  $obj->index();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>


    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>




</head>
<body>

<div style="height: 20px">
    <div id="message" class="btn-danger text-center" > <?php   echo Message::message();?>
    </div>
</div>





<div  class="container">

    <div class="nav navbar-default">
        <a href='create.php' class='btn btn-lg bg-success'>Create</a>

    </div>

    <div class="bg-info text-center"><h1>Profile Picture - Active List</h1></div>

    <table border="1px" class="table table-bordered table-striped">

        <tr>
            <th> Check All </th>
            <th> Serial </th>
            <th> ID </th>
            <th> Name </th>
            <th> Profile Pictures </th>
            <th> Action Buttons </th>

        </tr>


        <?php


        $serial=1;

        foreach ($allData as $oneData){

            if($serial%2) $bgColor = "AQUA";
            else $bgColor = "#ffffff";

            echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     <td style='padding-left: 4%'><input type='checkbox' class='checkbox' name='mark[]' value='$oneData->id'></td>
    
                                     <td style='width: 10%; text-align: center'>$serial</td>
                                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                                     <td style='width: 20%;'>$oneData->name</td>
                                     <td><a href='view.php?id=$oneData->id'> <img width='100px' height='100px' src='Uploads/$oneData->profile_picture'>  </a>  </td>
    
                                     <td>
                                       <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                                       <a href='edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                                       <a href='trash.php?id=$oneData->id' class='btn btn-warning'>Trash</a>
                                       <a href='delete.php?id=$oneData->id' class='btn btn-danger'>Delete</a>
                                       <a href='email.php?id=$oneData->id' class='btn btn-success'>Email</a>
    
                                     </td>
                                  </tr>
                              ";
            $serial++;

        }

        ?>

    </table>


</div>


<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>

</body>
</html>